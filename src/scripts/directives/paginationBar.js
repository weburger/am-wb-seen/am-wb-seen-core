/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialWeburgerSeen')

/**
 * @ngdoc directive
 * @memberof ngMaterialDashboard
 * @name amdPaginationScroll
 * @description # wbInfinateScroll
 * 
 */
.directive('wbSeenPaginationBar', function() {
	function postLink(scope, element, attr) {
		var query = {
				sortDesc: true,
				sortBy: scope.wbSeenSortKeys[0],
		};
		scope.wbSeenModel.setOrder(query.sortBy,'d');
		/*
		 * مرتب سازی مجدد داده‌ها بر اساس حالت فعلی 
		 */
		function reload(){
			if(!angular.isFunction(scope.wbSeenReload)){
				return;
			}
			scope.wbSeenReload(scope.wbSeenModel);
		}

		scope.$watch('query', function(){
			// Cecks sort key
			if(query.sortDesc){
				scope.wbSeenModel.setOrder(query.sortBy, 'd');
			} else {
				scope.wbSeenModel.setOrder(query.sortBy, 'a');
			}
			// Reloads search
			reload();
		}, true);

		if(angular.isFunction(scope.wbSeenReload)){
			scope.reload = reload;
		}
		scope.search = function(searchText){
			scope.wbSeenModel.setQuery(searchText);
			reload();
		}
		scope.query=query;
	}

	return {
		restrict : 'E',
		templateUrl: 'views/am-wb-seen-directives/paginationBar.html',
		scope : {
			wbSeenModel : '=',
			wbSeenReload : '=',
			wbSeenSortKeys: '=',
			wbSeenMoreActions: '='
		},
		link : postLink
	};
});
